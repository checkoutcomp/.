<?php
class ControllerCheckoutCallback extends Controller {
    private $error = array();

    public function index() {

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['orderId'])) {

            switch ($_POST['deliveryType']) {
                case 'pvz': $shipping_nethod = "Пункт выдачи: ".$_POST['address']; break;
                case 'postamat': $shipping_nethod = "Постамат: ".$_POST['address']; break;
                case 'express': $shipping_nethod = "Курьер Checkout";
                case 'express_own': $shipping_nethod = "Курьер Играю с Мамой";
            }

            $this->session->data['shipping_method'] =array('id' => $_POST['deliveryType'],
                                                           'title' => $shipping_nethod,
                                                           'cost' => $_POST['deliveryCost'],
                                                           'tax_class_id' => 0,
                                                           'text' => $_POST['deliveryCost'].'.00 р.'
                                                            );


            $total_data = array();
            $total = 0;
            $taxes = $this->cart->getTaxes();

            $this->load->model('checkout/extension');

            $sort_order = array();

            $results = $this->model_checkout_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['key'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                $this->load->model('total/' . $result['key']);

                $this->{'model_total_' . $result['key']}->getTotal($total_data, $total, $taxes);
            }

            $sort_order = array();

            foreach ($total_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $total_data);

            $data['store_id'] = $this->config->get('config_store_id');
            $data['store_name'] = $this->config->get('config_name');
            $data['store_url'] = $this->config->get('config_url');
            $data['customer_id'] = $this->customer->getId();
            $data['customer_group_id'] = 8;
            $data['firstname'] = $_POST['clientFIO'];
            $data['lastname'] = '';
            $data['email'] = $_POST['clientEmail'];
            $data['telephone'] = $_POST['clientPhone'];
            $data['fax'] = '';

            $data['shipping_firstname'] = $_POST['clientFIO'];
            $data['shipping_lastname'] = '';
            $data['shipping_company'] = '';
            $data['shipping_address_1'] = $_POST['address'];
            $data['shipping_address_2'] = '';
            $data['shipping_city'] = $_POST['deliveryPlace'];
            $data['shipping_postcode'] = $_POST['deliveryPostindex'];
            $data['shipping_zone'] = 'Москва';
            $data['shipping_zone_id'] = '2761';
            $data['shipping_country'] = 'Российская Федерация';
            $data['shipping_country_id'] = '176';
            $data['shipping_address_format'] = '';
            $data['shipping_method'] = $shipping_nethod;

            $data['payment_firstname'] = $_POST['clientFIO'];
            $data['payment_lastname'] = '';
            $data['payment_company'] = '';
            $data['payment_address_1'] = $_POST['address'];
            $data['payment_address_2'] = '';
            $data['payment_city'] = $_POST['deliveryPlace'];
            $data['payment_postcode'] = $_POST['deliveryPostindex'];
            $data['payment_zone'] = 'Москва';
            $data['payment_zone_id'] = '2761';
            $data['payment_country'] = 'Российская Федерация';
            $data['payment_country_id'] = '176';
            $data['payment_address_format'] = '';
            $data['payment_method'] = 'Оплата при доставке';

            $product_data = array();

            foreach ($this->cart->getProducts() as $product) {
                $option_data = array();

                foreach ($product['option'] as $option) {
                    $option_data[] = array(
                        'product_option_value_id' => $option['product_option_value_id'],
                        'name'                    => $option['name'],
                        'value'                   => $option['value'],
                        'prefix'                  => $option['prefix']
                    );
                }

                $product_data[] = array(
                    'product_id' => $product['product_id'],
                    'name'       => $product['name'],
                    'model'      => $product['model'],
                    'option'     => $option_data,
                    'download'   => $product['download'],
                    'quantity'   => $product['quantity'],
                    'subtract'   => $product['subtract'],
                    'price'      => $product['price'],
                    'total'      => $product['total'],
                    'tax'        => $this->tax->getRate($product['tax_class_id'])
                );
            }

            $data['products'] = $product_data;
            $data['totals'] = $total_data;
            $data['comment'] = $_POST['comment'];
            $data['total'] = $total;
            $data['language_id'] = 1;
            $data['currency_id'] = 1;
            $data['currency'] = $this->currency->getCode();
            $data['value'] = $this->currency->getValue($this->currency->getCode());

            if (isset($this->session->data['coupon'])) {
                $this->load->model('checkout/coupon');

                $coupon = $this->model_checkout_coupon->getCoupon($this->session->data['coupon']);

                if ($coupon) {
                    $data['coupon_id'] = $coupon['coupon_id'];
                } else {
                    $data['coupon_id'] = 0;
                }
            } else {
                $data['coupon_id'] = 0;
            }

            $data['ip'] = $this->request->server['REMOTE_ADDR'];

            $this->load->model('checkout/order');

            $this->session->data['order_id'] = $this->model_checkout_order->create($data);

            $this->model_checkout_order->confirm($this->session->data['order_id'], 1, 'Checkout');

            /*$tuCurl = curl_init();
            curl_setopt($tuCurl, CURLOPT_URL, "http://platform.checkout.ru/service/login/ticket/SNFqpcywQsSFXrGvavx8Vc4d");
            curl_setopt($tuCurl, CURLOPT_VERBOSE, 0);
            curl_setopt($tuCurl, CURLOPT_HEADER, 0);
            curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
            $tuData = curl_exec($tuCurl);
            if(!curl_errno($tuCurl)){
                $info = curl_getinfo($tuCurl);
            } else {
                echo 'Curl error: ' . curl_error($tuCurl);
            }
            curl_close($tuCurl);
            $responce = json_decode($tuData,true);
            $ticket = $responce["ticket"];

            $dat = array(
                "apiKey" => 'SNFqpcywQsSFXrGvavx8Vc4d',
                "order" => array(
                    "shopOrderId" => $this->session->data['order_id'],
                    "paymentMethod" => 'cash'
                )
            );
            require($_SERVER["DOCUMENT_ROOT"] . "/httpful.phar");

            $response = \Httpful\Request::post("http://platform.checkout.ru/service/order/".$_POST['orderId']) ->sendsJson() ->body(json_encode($dat)) ->sendIt();
                        if ($response->hasErrors()) {
                            $headers = explode("\r\n", $response->raw_headers);
                            $header = explode(" ", $headers[0]);
                            $response = str_replace($header[0], "", $headers[0]);
                        } else {
                            $response = $response->body;
                        }
            */
            $this->redirect(HTTPS_SERVER . 'index.php?route=checkout/success');

        }
    }
}